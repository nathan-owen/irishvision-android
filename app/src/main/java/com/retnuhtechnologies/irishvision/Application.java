package com.retnuhtechnologies.irishvision;

import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseConfig;
import com.parse.ParseCrashReporting;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;

/**
 * Created by nathanowen on 3/4/15.
 */
public class Application extends android.app.Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "RHKTMgj3dGa2Z0C6hNaIzhJd2";
    private static final String TWITTER_SECRET = "5Or1VrMhDgU9E1a0DP4LhQfwQzXpS2fWkTfaSF1XEoorwNnjcc";

    private static final long configRefreshInterval = 12 * 60 * 60 * 1000;
    private static long lastFetchedTime;
    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        ParseCrashReporting.enable(this);

        Parse.enableLocalDatastore(this);

        Parse.initialize(this, "RZHFvyqThg1MWCjlWB4jrSV0WC9Gdbl1y6owmMlZ", "lRpF7MiTy92zCcg1BOuoFXzWSOvwea4B1o0Pl2An");
        ParseInstallation.getCurrentInstallation().saveInBackground();


        ParsePush.subscribeInBackground("Everyone");
refreshConfig();



    }
    // Fetches the config at most once every 12 hours per app runtime
    public static void refreshConfig() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - lastFetchedTime > configRefreshInterval) {
            lastFetchedTime = currentTime;
            ParseConfig.getInBackground();
        }
    }
}
