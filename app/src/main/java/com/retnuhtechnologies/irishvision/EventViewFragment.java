package com.retnuhtechnologies.irishvision;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Date;

/**
 * Created by nathanowen on 3/22/15.
 */
public class EventViewFragment extends Fragment {

    private static final String TAG = EventViewFragment.class.getSimpleName();
    protected TextView mLocation;
    protected TextView mDate;
    protected TextView mDetails;
    protected TextView mIrishScoreField;
    protected TextView mOppScoreField;
    protected TextView mGameStateField;
    protected TextView mOpposingTeamField;

    protected String mEventTitle;
    protected String mDateString;
    protected String mLocationString;
    protected String  mDetailsString;
    protected Date mDateDate;
    protected String mOppScore;
    protected String mIrishScore;
    protected String mOpposingTeam;
    protected String mGameState;

    protected Button mAd;

    public static final long HOUR = 3600*1000; // in milli-seconds.

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_event_view, container, false);
        mLocation = (TextView)rootView.findViewById(R.id.locationText);
        mDate = (TextView)rootView.findViewById(R.id.dateText);
        mIrishScoreField = (TextView)rootView.findViewById(R.id.irishScore);
        mOppScoreField = (TextView)rootView.findViewById(R.id.oppScore);
        mGameStateField = (TextView)rootView.findViewById(R.id.gameState);
        mOpposingTeamField = (TextView) rootView.findViewById(R.id.oppTeamText);

        Bundle bundle = this.getArguments();
        mDateString = bundle.getString(ParseConstants.BUNDLE_EVENT_DATE);
        mLocationString = bundle.getString(ParseConstants.BUNDLE_EVENT_LOCATION);
        mDateDate = (Date)bundle.getSerializable(ParseConstants.BUNDLE_EVENT_DATE_DATE_TYPE);
        mOppScore = String.valueOf(bundle.getInt(ParseConstants.BUNDLE_OPP_SCORE));
        mIrishScore = String.valueOf(bundle.getInt(ParseConstants.BUNDLE_IRISH_SCORE));
        mOpposingTeam = bundle.getString(ParseConstants.BUNDLE_OPP_TEAM);
        mGameState = bundle.getString(ParseConstants.BUNDLE_GAME_STATE);

        mEventTitle = bundle.getString(ParseConstants.BUNDLE_EVENT_TITLE);
        mLocation.setText(mLocationString);
        mDate.setText(mDateString);
        mIrishScoreField.setText(mIrishScore);
        mOpposingTeamField.setText(mOpposingTeam);
        mOppScoreField.setText(mOppScore);
        mGameStateField.setText(mGameState);



        setHasOptionsMenu(true);

        return rootView;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_map) {
            openMap();
            return true;
        }
        else if(id == R.id.action_calendar) {
            openCalendar();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private void openMap() {
        Log.i(TAG,"Map Opened");
        String address = mLocationString;
        String map = "http://maps.google.co.in/maps?q=" + address;

        address = address.replace(" ", "+");
        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + address);

        Intent openMaps = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        openMaps.setPackage("com.google.android.apps.maps");
        if (openMaps.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(openMaps);
        }
        else{
            Intent browser = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
            startActivity(browser);
        }
    }

        private void openCalendar() {
        Log.i(TAG,"Calendar Opened");
        long beginInMillis = mDateDate.getTime();
        Date endDate = new Date(mDateDate.getTime() + 1 * HOUR);

        long endInMillis =endDate.getTime();

        Log.i(TAG, "Start Time: " + mDateDate + " End Time: " + endDate);
        Log.i(TAG, "Start Time: " + beginInMillis + " End Time: " + endInMillis);

        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginInMillis)
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endInMillis)
                .putExtra(CalendarContract.Events.TITLE, mEventTitle)
                .putExtra(CalendarContract.Events.DESCRIPTION, mDetailsString)
                .putExtra(CalendarContract.Events.EVENT_LOCATION, mLocationString);

        startActivity(intent);


    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {


            inflater.inflate(R.menu.menu_schedule, menu);



        super.onCreateOptionsMenu(menu,inflater);

    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(mEventTitle);
    }


}


