package com.retnuhtechnologies.irishvision;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;
import com.twitter.sdk.android.tweetui.UserTimeline;

/**
 * Created by nathanowen on 3/22/15.
 */
public class IrishVisionTwitterFragment extends ListFragment {
    private static final String TAG = IrishVisionTwitterFragment.class.getSimpleName();






    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_twitter, container,false);

        final UserTimeline userTimeline = new UserTimeline.Builder()
                .screenName("IrishVisionHD")
                .build();
        final TweetTimelineListAdapter adapter = new TweetTimelineListAdapter(getActivity(), userTimeline);
        setListAdapter(adapter);

        setHasOptionsMenu(false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            return true;
        }



        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_refresh, menu);



        super.onCreateOptionsMenu(menu,inflater);

    }




    @Override
    public void onResume() {
        super.onResume();
            getActivity().setTitle("Twitter - @IrishVisionHD");


    }

}
