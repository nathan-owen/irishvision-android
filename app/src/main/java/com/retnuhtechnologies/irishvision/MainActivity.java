package com.retnuhtechnologies.irishvision;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.parse.ParsePush;
import com.retnuhtechnologies.irishvision.R;
import com.retnuhtechnologies.irishvision.adapter.NavDrawerListAdapter;
import com.retnuhtechnologies.irishvision.model.NavDrawerItem;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private Fragment mFragment;
    //nav drawer title
    private CharSequence mDrawerTitle;
    //used to store app title
    private CharSequence mTitle;
    SharedPreferences prefs = null;
    //slide menu items
    private String[] mNavMenuTitles;

    private ArrayList<NavDrawerItem> mNavDrawerItems;
    private NavDrawerListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitle = mDrawerTitle = getTitle();


        prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);





        //determine if is sender staff or logged out
        //change this variable
        //load slide menu items
        loadSlideMenuItems();


        //enabling action bar app icon and behaving it as toggle button


        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mTitle);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(mDrawerTitle);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        if (savedInstanceState == null) {
            displayView(0);
        }


        mDrawerLayout.openDrawer(mDrawerList);
        getSupportActionBar().setTitle(getString(R.string.app_name));
        if (prefs.getBoolean("firstrun", true)) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Introducing LiveScore!")
                    .setMessage("With LiveScore you can receive live score notifications from the Games!\nWould you like to receive live score updates?\nYou can turn them off later if you want, in the Settings Menu.")
                    .setCancelable(true)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ParsePush.subscribeInBackground("LiveScore");

                            dialog.cancel();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            prefs.edit().putBoolean("firstrun",false).apply();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();


    }



    public void loadSlideMenuItems() {


           mNavMenuTitles = getResources().getStringArray(R.array.menuItems);
//


       mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
 mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
       mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
        mNavDrawerItems = new ArrayList<NavDrawerItem>();

//        //add nav drawer items
        for (int i = 0; i < mNavMenuTitles.length; i++) {
            mNavDrawerItems.add(new NavDrawerItem(mNavMenuTitles[i]));
        }

        //setting the nav drawer list adapter
        mAdapter = new NavDrawerListAdapter(getApplicationContext(),
                mNavDrawerItems);
        mDrawerList.setAdapter(mAdapter);

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }




    private void displayView(int position) {

        switch (position) {

            case 0:

                mFragment = new VideosFragment();
                Log.i(TAG,mFragment + " ");
                break;
            case 1:
                mFragment = new ScheduleFragment();
                break;
            case 2:
                mFragment = new RosterFragment();
                break;

            case 3:
                mFragment = new TwitterFragment();
                break;
            case 4:
                mFragment = new IrishVisionTwitterFragment();
                break;
            case 5:
                Intent intent;
                String url = "https://www.youtube.com/channel/UCcb2EqJW_HwW8kQrEmIwCsg";
                try {
                    intent =new Intent(Intent.ACTION_VIEW);
                    intent.setPackage("com.google.android.youtube");
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                }
            case 6:
                mFragment = new SettingsFragment();
                break;

//            case 6:
//                mFragment = new SendMessageFragment();
//                break;
//            case 7:
//                mFragment = new ParkingAssistantFragment();
//                break;
//            case 8:
//                mFragment = new SignInFragment();
                //break;
            default:
                break;

        }
        if (mFragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_container, mFragment,mFragment.getClass().getSimpleName());
            fragmentTransaction.addToBackStack(null);
            //updateSelectedItem and title then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(mNavMenuTitles[position]);
            fragmentTransaction.commit();

            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            //error creating fragment
            Log.e(TAG, "Error in Creating the Fragment");
        }

    }

    private class SlideMenuClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            displayView(position);
        }
    }


    @Override
    public void onBackPressed() {
        if(mFragment != null) {
            if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                mDrawerLayout.closeDrawer(mDrawerList);
            }
            else if (mFragment.isResumed()) {

                mDrawerLayout.openDrawer(mDrawerList);
                getSupportActionBar().setTitle(mDrawerTitle);


            }   else {
                getFragmentManager().popBackStack();
            }
        }
    }




//  @Override
//  public void onListClicked(ParseObject object){
//      Log.i(TAG,object.getString(ParseConstants.KEY_ORGANIZATION));
//     OrganizationFragment fragment = (OrganizationFragment) getFragmentManager().findFragmentByTag()
//
//  }


}
