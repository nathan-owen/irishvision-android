package com.retnuhtechnologies.irishvision;

/**
 * Created by nathanowen on 3/4/15.
 */
public class ParseConstants {
    //Class Names
    public static final String CLASS_ROSTER = "Roster";
    public static final String CLASS_SCHEDULE = "Schedule";
    public static final String CLASS_VIDEOS = "Videos";


    //Field Name
    public static final String KEY_VIDEO_UPL_DATE = "video_upl_date";
    public static final String KEY_VIDEO_NAME = "video_name";
    public static final String KEY_VIDEO_URL = "video_url";
    public static final String KEY_IMG_URL = "img_url";
    public static final String KEY_TITLE = "Title";
    public static final String KEY_CREATED_AT = "createdAt";
    public static final String KEY_DATE = "Date";
    public static final String KEY_BOYS = "Boys";
    public static final String KEY_VARSITY = "Varsity";

    public static final String KEY_LOCATION = "location";

    public static final String KEY_NUMBER = "number";
    public static final String KEY_POSITIONS = "Positions";
    public static final String KEY_GRADE = "year";
    public static final String KEY_FIRST_NAME = "firstName";
    public static final String KEY_LAST_NAME = "lastName";
    public static final String KEY_TYPE = "type";
    public static final String KEY_COACH_TITLE = "coachTitle";

    public static String KEY_DETAILS = "details";
    public static final String KEY_IMG_FILE = "thumb_img";






    //Bundle Names
    public static final String BUNDLE_EVENT_LOCATION = "location";
    public static final String BUNDLE_EVENT_DETAILS = "details";
    public static final String BUNDLE_EVENT_DATE = "Date";
    public static final String BUNDLE_EVENT_TITLE = "Title";
    public static final String BUNDLE_EVENT_DATE_DATE_TYPE = "eventDateLong";
    public static final String BUNDLE_VIDEO_NAME = "video_name";
    public static final String BUNDLE_VIDEO_UPL_DATE = "video_upl_date";
    public static final String BUNDLE_VIDEO_URL = "vidURL";
    public static final String BUNDLE_IMG_URL = "imgURL";
    public static final String BUNDLE_IMG_FILE = "thumb_img";
    public static final String BUNDLE_OPP_SCORE = "oppScore";
    public static final String BUNDLE_IRISH_SCORE = "irishScore";
    public static final String BUNDLE_GAME_STATE = "gameState";
    public static final String BUNDLE_OPP_TEAM = "opposingTeam";


    public class KEY_TYPE {
    }

    public class KEY_IMG_FILE {
    }


}


