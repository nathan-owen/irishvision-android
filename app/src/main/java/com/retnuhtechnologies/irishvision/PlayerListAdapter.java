package com.retnuhtechnologies.irishvision;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.parse.ParseObject;

import java.util.List;

/**
 * Created by nathanowen on 3/2/15.
 */
public class PlayerListAdapter extends BaseAdapter {
    private static final String TAG = PlayerListAdapter.class.getSimpleName();
    private Context context;
    private List<ParseObject> rosterList;


    public PlayerListAdapter(Context context, List<ParseObject> coachesList) {
        super();
        this.rosterList = coachesList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return rosterList.size();
    }

    @Override
    public Object getItem(int position) {
        return rosterList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      View rowView;
        String firstName = rosterList.get(position).getString(ParseConstants.KEY_FIRST_NAME).toLowerCase();
        String lastName = rosterList.get(position).getString(ParseConstants.KEY_LAST_NAME).toLowerCase();
        firstName = firstName.substring(0,1).toUpperCase() + firstName.substring(1);
        lastName = lastName.substring(0,1).toUpperCase() + lastName.substring(1);
        String type = rosterList.get(position).getString(ParseConstants.KEY_TYPE);
if(type.equals("player")) {
    rowView = LayoutInflater.from(context).
            inflate(R.layout.roster_row, parent, false);

    TextView playerName = (TextView) rowView.findViewById(R.id.playerName);
    TextView playerNumber = (TextView) rowView.findViewById(R.id.playerNumber);
    TextView playerGrade = (TextView) rowView.findViewById(R.id.playerGrade);
    TextView playerPosition = (TextView) rowView.findViewById(R.id.playerPosition);


    Log.i(TAG, firstName + lastName);


    playerName.setText((firstName + " " + lastName));
    if (rosterList.get(position).getNumber(ParseConstants.KEY_NUMBER) != null) {
        playerNumber.setText(rosterList.get(position).getNumber(ParseConstants.KEY_NUMBER).toString());
    }
    if (rosterList.get(position).getString(ParseConstants.KEY_GRADE) != null) {

        playerGrade.setText(rosterList.get(position).getString(ParseConstants.KEY_GRADE));
    }
    if (rosterList.get(position).getString(ParseConstants.KEY_POSITIONS) != null) {

        playerPosition.setText(rosterList.get(position).getString(ParseConstants.KEY_POSITIONS));
    }
}
        else{
    rowView = null;
        }
        //endregion

        return rowView;
    }



}




