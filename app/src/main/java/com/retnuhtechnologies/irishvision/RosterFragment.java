package com.retnuhtechnologies.irishvision;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.commonsware.cwac.merge.MergeAdapter;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by nathanowen on 3/22/15.
 */
public class RosterFragment extends ListFragment {
    private static final String TAG = RosterFragment.class.getSimpleName();
    private Button mAd;
    private String mSport;
    private Boolean mBoys;
    protected List<ParseObject> mPlayers;
    protected List<ParseObject>mCoaches;
    protected List<ParseObject>mStudentManagers;

    private MergeAdapter adapter=null;



    protected RosterListAdapter mPlayerAdapter;
    private String mSportTitle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_roster, container, false);
mCoaches = new ArrayList<>();
        mPlayers = new ArrayList<>();
        mStudentManagers = new ArrayList<>();

setHasOptionsMenu(false);

        getPlayersLocally();
        return rootView;
    }

    private void getPlayers() {

        getListView().invalidateViews();
        ParseQuery<ParseObject> query ;
             query = ParseQuery.getQuery("Roster");


        query.addAscendingOrder(ParseConstants.KEY_TYPE);
        query.addAscendingOrder(ParseConstants.KEY_NUMBER);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> parseObjects, ParseException e) {
                if (e == null) {

                        ParseObject.unpinAllInBackground(new DeleteCallback() {
                            @Override
                            public void done(ParseException e) {
                                ParseObject.pinAllInBackground("roster",parseObjects);
                            }
                        });
                    Log.i(TAG,mCoaches.toString());
mCoaches.clear();
                    mPlayers.clear();
                    mStudentManagers.clear();
                 for(ParseObject member: parseObjects) {
                     Log.i(TAG,member +"");
                     if (member.getString(ParseConstants.KEY_TYPE).equals("coach")) {

                        mCoaches.add(member);
                     } else if (member.getString(ParseConstants.KEY_TYPE).equals("student_manager")){
                         mStudentManagers.add(member);

                     }
                     else{
                         mPlayers.add(member);

                     }
                 }
                    adapter = null;

                   setListAdapter(null);
                    adapter = new MergeAdapter();

                    adapter.addView(coachesLabel());
                    adapter.addAdapter(coachesList());

                    adapter.addView(playersLabel());
                    adapter.addAdapter(playersList());

                    adapter.addView(studentManagersLabel());
                    adapter.addAdapter(studentManagersList());

                    adapter.notifyDataSetChanged();

                    setListAdapter(adapter);
                    getListView().invalidateViews();

                }

//

//


                 else {
                    //Error
                    Log.e(TAG, e.getMessage());
                    AlertDialog.Builder builder = new AlertDialog.Builder(getListView().getContext());
                    builder.setMessage(e.getMessage())
                            .setTitle(R.string.error_title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });
    }

    private void getPlayersLocally() {

        ParseQuery<ParseObject> query ;
        query = ParseQuery.getQuery("Roster");
        query.fromLocalDatastore();

        query.addAscendingOrder(ParseConstants.KEY_TYPE);
        query.addAscendingOrder(ParseConstants.KEY_NUMBER);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e == null) {
                    if(checkConnection()){
                        getPlayers();
                    }
                    Log.i(TAG,mCoaches.toString());
                    for(ParseObject member: parseObjects) {
                        Log.i(TAG,member +"");
                        if (member.getString(ParseConstants.KEY_TYPE).equals("coach")) {

                            mCoaches.add(member);
                        } else if (member.getString(ParseConstants.KEY_TYPE).equals("student_manager")){
                            mStudentManagers.add(member);

                        }
                        else{
                            mPlayers.add(member);

                        }
                    }
                    adapter = new MergeAdapter();

                    adapter.addView(coachesLabel());
                    adapter.addAdapter(coachesList());

                    adapter.addView(playersLabel());
                    adapter.addAdapter(playersList());

                    adapter.addView(studentManagersLabel());
                    adapter.addAdapter(studentManagersList());

                    setListAdapter(adapter);
                }

//

//


                else {
                    //Error
                    Log.e(TAG, e.getMessage());
                    AlertDialog.Builder builder = new AlertDialog.Builder(getListView().getContext());
                    builder.setMessage(e.getMessage())
                            .setTitle(R.string.error_title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });
    }

    private boolean checkConnection(){
        ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();


    }

    @Override
    public void onResume() {
        super.onResume();
            getActivity().setTitle("Roster");

    }

    private ListAdapter playersList(){


        return(new PlayerListAdapter(
                getActivity(),
               mPlayers));
    }
    private ListAdapter coachesList(){


        return(new CoachesListAdapter(
                getActivity(),
                mCoaches));
    }
    private ListAdapter studentManagersList(){


        return(new CoachesListAdapter(
                getActivity(),
                mStudentManagers));
    }

    private View coachesLabel() {


        return getActivity().getLayoutInflater().inflate(R.layout.label_coaches,null);
    }

    private View playersLabel() {
        return getActivity().getLayoutInflater().inflate(R.layout.label_players,null);

    }
    private View studentManagersLabel() {

        return getActivity().getLayoutInflater().inflate(R.layout.label_student_managers,null);
    }



}
