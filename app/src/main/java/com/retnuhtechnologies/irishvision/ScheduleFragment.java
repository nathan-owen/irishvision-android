package com.retnuhtechnologies.irishvision;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.DeleteCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by nathanowen on 3/22/15.
 */
public class ScheduleFragment extends ListFragment {
    private static final String TAG = ScheduleFragment.class.getSimpleName();
    private boolean loadedFromNetwork;
    protected TextView mNoMessagesTextView;



    private ScheduleParseQueryAdapter customAdapter;
    private String mSportTitle;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_schedule, container,false);

        mNoMessagesTextView = (TextView) rootView.findViewById(R.id.noMessages);
        getGamesLocally();
        setHasOptionsMenu(true);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

private void getGamesLocally(){
    customAdapter = new ScheduleParseQueryAdapter(getActivity(),"Schedule",true);
setListAdapter(customAdapter);
customAdapter.loadObjects();
    customAdapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<ParseObject>() {
        @Override
        public void onLoading() {

        }

        @Override
        public void onLoaded(final List<ParseObject> parseObjects, Exception e) {
            if (e == null) {
                if(checkConnection()){
                    getGames();
                }
                if (parseObjects.isEmpty()) {
                    mNoMessagesTextView.setText("There are Currently No Scheduled Events");
                } else {

                    mNoMessagesTextView.setText("");
                }
            }

        }

    });
}

    private boolean checkConnection(){
        ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();


    }

    private void getGames() {
        loadedFromNetwork = true;
        customAdapter = new ScheduleParseQueryAdapter(getActivity(),"Schedule",false);
        setListAdapter(customAdapter);
        customAdapter.loadObjects();
        customAdapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<ParseObject>() {
            @Override
            public void onLoading() {

            }

            @Override
            public void onLoaded(final List<ParseObject> parseObjects, Exception e) {
                if (e == null) {
                                        if (parseObjects.isEmpty()) {
                        mNoMessagesTextView.setText("There are Currently No Scheduled Events");
                    } else {
                                            ParseObject.unpinAllInBackground(new DeleteCallback() {
                                                @Override
                                                public void done(ParseException e) {
                                                    ParseObject.pinAllInBackground("schedule",parseObjects);
                                                }
                                            });
                        mNoMessagesTextView.setText("");
                    }
                }

            }

        });
    }

//
//
//
//

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        ParseObject organization = customAdapter.getItem(position);

        Fragment newFragment = new EventViewFragment();
        SimpleDateFormat format = new SimpleDateFormat("EEE', 'MMM d', 'yyyy 'at' hh':'mm a ");
        format.setTimeZone(TimeZone.getDefault());


        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container,newFragment);
        fragmentTransaction.addToBackStack(null);
        //updateSelectedItem and title then close the drawer
        getActivity().setTitle(organization.getString(ParseConstants.KEY_TITLE));
        Bundle bundle = new Bundle();

        bundle.putString(ParseConstants.BUNDLE_EVENT_TITLE,organization.getString(ParseConstants.KEY_TITLE));
        bundle.putString(ParseConstants.BUNDLE_EVENT_DATE,format.format(organization.getDate(ParseConstants.KEY_DATE)));
        bundle.putString(ParseConstants.BUNDLE_EVENT_LOCATION,organization.getString(ParseConstants.KEY_LOCATION));
        bundle.putString(ParseConstants.BUNDLE_EVENT_DETAILS,organization.getString(ParseConstants.KEY_DETAILS));
        bundle.putSerializable(ParseConstants.BUNDLE_EVENT_DATE_DATE_TYPE, organization.getDate(ParseConstants.KEY_DATE));
        bundle.putInt(ParseConstants.BUNDLE_OPP_SCORE, organization.getInt(ParseConstants.BUNDLE_OPP_SCORE));
        bundle.putInt(ParseConstants.BUNDLE_IRISH_SCORE,organization.getInt(ParseConstants.BUNDLE_IRISH_SCORE));
        bundle.putString(ParseConstants.BUNDLE_GAME_STATE, organization.getString(ParseConstants.BUNDLE_GAME_STATE));
        bundle.putString(ParseConstants.BUNDLE_OPP_TEAM,organization.getString(ParseConstants.BUNDLE_OPP_TEAM));


        newFragment.setArguments(bundle);

        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

    private void refresh() {
        if(loadedFromNetwork){
            customAdapter.loadObjects();
        }
        else {
            getGamesLocally();
        }
        Log.i(TAG, "refreshed");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_refresh, menu);



        super.onCreateOptionsMenu(menu,inflater);

    }




    @Override
    public void onResume() {
        super.onResume();
            getActivity().setTitle("Schedule");

        refresh();

    }

}
