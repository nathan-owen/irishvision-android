package com.retnuhtechnologies.irishvision;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by nathanowen on 3/4/15.
 */
public class ScheduleParseQueryAdapter extends ParseQueryAdapter<ParseObject>{


    private static final String TAG = ScheduleParseQueryAdapter.class.getSimpleName();
    private  static String filter = null;

    public ScheduleParseQueryAdapter(Context context, final String channel, final Boolean varsity, final Boolean boys) {
        // Use the QueryFactory to construct a PQA that will only show
        // Todos marked as high-pri
        super(context, new QueryFactory<ParseObject>() {
            public ParseQuery create() {
                ParseQuery query = new ParseQuery(channel);
                query.whereEqualTo(ParseConstants.KEY_VARSITY,varsity);
                query.whereEqualTo(ParseConstants.KEY_BOYS,boys);
                query.orderByDescending(ParseConstants.KEY_DATE);
                return query;
            }
        });
        setObjectsPerPage(25);
    }

    public ScheduleParseQueryAdapter(Context context, final String channel,final boolean local) {
        // Use the QueryFactory to construct a PQA that will only show
        // Todos marked as high-pri
        super(context, new QueryFactory<ParseObject>() {
            public ParseQuery create() {
                ParseQuery query = new ParseQuery(channel);
if(local){
    query.fromLocalDatastore();
}
                query.orderByAscending(ParseConstants.KEY_DATE);
                return query;
            }
        });
        setObjectsPerPage(25);
    }


    // Customize the layout by overriding getItemView
    @Override
    public View getItemView(ParseObject object, View v, ViewGroup parent) {
        if (v == null) {
            v = View.inflate(getContext(), R.layout.video_row, null);
        }

        super.getItemView(object, v, parent);



        // Add the title view
        TextView titleTextView = (TextView) v.findViewById(R.id.titleText);
        titleTextView.setText(object.getString(ParseConstants.KEY_TITLE));

        // Add a reminder of how long this item has been outstanding
        TextView timestampView = (TextView) v.findViewById(R.id.dateText);
        Date date = object.getDate(ParseConstants.KEY_DATE);
        Log.i(TAG, String.valueOf(date));
        SimpleDateFormat format = new SimpleDateFormat("EEE', 'MMM d', 'yyyy 'at' hh':'mm a ");
        format.setTimeZone(TimeZone.getDefault());

        timestampView.setText(format.format(date));
        return v;
    }
}
