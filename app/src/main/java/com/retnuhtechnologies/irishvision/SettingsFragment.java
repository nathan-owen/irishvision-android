package com.retnuhtechnologies.irishvision;

import android.app.Fragment;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;
import com.twitter.sdk.android.tweetui.UserTimeline;

/**
 * Created by nathanowen on 3/22/15.
 */
public class SettingsFragment extends Fragment {
    private static final String TAG = SettingsFragment.class.getSimpleName();




    protected Switch mLiveScoreToggle;
    protected Boolean recieveLiveScore;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_settings, container,false);
        mLiveScoreToggle = (Switch) rootView.findViewById(R.id.liveScoreToggle);
        recieveLiveScore = ParseInstallation.getCurrentInstallation().getList("channels").contains("LiveScore");
        mLiveScoreToggle.setChecked(recieveLiveScore);
        mLiveScoreToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(recieveLiveScore){
                    recieveLiveScore = false;
                }
                else{
                    recieveLiveScore = true;
                }
                setLiveScoreSubscription();
            }
        });

        setHasOptionsMenu(false);

        return rootView;
    }

    private void setLiveScoreSubscription() {

        if(recieveLiveScore){
            ParsePush.subscribeInBackground("LiveScore");
        }
        else{
            ParsePush.unsubscribeInBackground("LiveScore");

        }

    }




    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }








    @Override
    public void onResume() {
        super.onResume();
            getActivity().setTitle("Settings");


    }

}
