package com.retnuhtechnologies.irishvision;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.parse.ParseObject;

import java.util.List;

/**
 * Created by nathanowen on 3/2/15.
 */
public class StudentManagerListAdapter extends BaseAdapter {
    private static final String TAG = StudentManagerListAdapter.class.getSimpleName();
    private Context context;
    private List<ParseObject> rosterList;


    public StudentManagerListAdapter(Context context, List<ParseObject> coachesList) {
        super();
        this.rosterList = coachesList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return rosterList.size();
    }

    @Override
    public Object getItem(int position) {
        return rosterList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      View rowView;
        String firstName = rosterList.get(position).getString(ParseConstants.KEY_FIRST_NAME).toLowerCase();
        String lastName = rosterList.get(position).getString(ParseConstants.KEY_LAST_NAME).toLowerCase();
        firstName = firstName.substring(0,1).toUpperCase() + firstName.substring(1);
        lastName = lastName.substring(0,1).toUpperCase() + lastName.substring(1);
            rowView = LayoutInflater.from(context).
                    inflate(R.layout.coach_row, parent, false);
            TextView name = (TextView) rowView.findViewById(R.id.coachName);
            TextView title = (TextView) rowView.findViewById(R.id.coachPosition);






                String coachTitle = rosterList.get(position).getString(ParseConstants.KEY_COACH_TITLE);

                name.setText(firstName + " " + lastName);
                title.setText(coachTitle);

        //endregion

        return rowView;
    }



}




