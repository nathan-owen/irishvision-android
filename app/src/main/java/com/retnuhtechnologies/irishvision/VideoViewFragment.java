package com.retnuhtechnologies.irishvision;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.media.MediaPlayer;
import android.media.session.MediaController;
import android.net.Uri;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nathanowen on 3/22/15.
 */
public class VideoViewFragment extends Fragment {

    private static final String TAG = VideoViewFragment.class.getSimpleName();
    protected TextView mVideoNameLbl;
    protected TextView mVideoReleaseDateLbl;
    protected ImageView mVideoThumb;
    protected TextView mDetails;
    protected Uri mVideoUrl;
    protected String mVideoName;
    protected String mVideoUplDate;
    protected Uri mThumbUrl;
    protected String mThumbString;
    protected String mVidString;
    private ProgressDialog prgDialog;
    protected String mFileName;
    protected boolean mFileDownloading = false;
    DownloadVideoTask downloadTask = null;
    private boolean fileIsDownloaded;
    public static final int progress_bar_type = 0;
    private byte[] mImgBytes;

    //Download Variables
    private final int TIMEOUT_CONNECTION = 5000;//5 Seconds
    private final int TIMEOUT_SOCKET = 30000;//30 Seconds


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_video_view, container, false);
        mVideoNameLbl = (TextView) rootView.findViewById(R.id.video_name_lbl);
        mVideoReleaseDateLbl = (TextView) rootView.findViewById(R.id.video_upl_date_lbl);
        mVideoThumb = (ImageView) rootView.findViewById(R.id.video_thumb);
//        mLocation = (TextView)rootView.findViewById(R.id.locationText);
//        mDate = (TextView)rootView.findViewById(R.id.dateText);
//        mDetails = (TextView)rootView.findViewById(R.id.event_details_text);
        Bundle bundle = this.getArguments();
        mVideoName = bundle.getString(ParseConstants.BUNDLE_VIDEO_NAME);
        mVideoUplDate = bundle.getString(ParseConstants.BUNDLE_VIDEO_UPL_DATE);
        mVideoUrl = Uri.parse(bundle.getString(ParseConstants.BUNDLE_VIDEO_URL));
        mVidString = bundle.getString(ParseConstants.BUNDLE_VIDEO_URL);
        mImgBytes = bundle.getByteArray(ParseConstants.BUNDLE_IMG_FILE);
        mThumbUrl = Uri.parse(bundle.getString(ParseConstants.BUNDLE_IMG_URL));
        mThumbString = (bundle.getString(ParseConstants.BUNDLE_IMG_URL));
        mFileName = mVideoName.replace(" ", "_").replace("-", "_") + ".mp4".toLowerCase();

       setUI();

        setHasOptionsMenu(true);


        return rootView;
    }


    private void playInNewWindow() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);

        if (isFileDownloaded()) {

            File video = new File(getActivity().getFilesDir() + "/" + mFileName);
            String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(".mp4");
            Log.i(TAG, "File: " + video);

            video.setReadable(true, false);
            if (video.exists()) {
                Log.i(TAG, "Playing Local");
                intent.setDataAndType(Uri.fromFile(video), "video/*");

            }
        } else {
            intent.setDataAndType(mVideoUrl, "video/*");
            Log.i(TAG, "Playing Remote");

        }
Map<String,String> dimensions = new HashMap<String,String>();
        dimensions.put("video",mVideoName);
        ParseAnalytics.trackEventInBackground("watched",dimensions);
        startActivity(intent);
    }

    //region Setup
    private void setUI() {
        mVideoNameLbl.setText(mVideoName);
        mVideoReleaseDateLbl.setText(mVideoUplDate);
        getThumb();
        mVideoThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playInNewWindow();
            }
        });

    }

//    private void getThumb() {
//        ImageLoader imageLoader = ImageLoader.getInstance();
//        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
//        imageLoader.displayImage(mThumbString, mVideoThumb); // Default options will be used
//
//// Load image, decode it to Bitmap and return Bitmap to callback
//        imageLoader.loadImage(mThumbString, new SimpleImageLoadingListener() {
//            @Override
//            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                // Do whatever you want with Bitmap
//            }
//        });
//
//
//    }
    private void getThumb(){


                    mVideoThumb.setImageBitmap(BitmapFactory.decodeByteArray(mImgBytes,0,mImgBytes.length));

    }

    private boolean isFileDownloaded() {

        File video = new File(getActivity().getFilesDir() + "/" + mFileName);
        if (video.exists()) {

            return true;


        } else {
            return false;
        }
    }

    //endregion
    //region OnRes, On OpISelectm OnCoM
    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(mVideoName);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_download) {
            downloadVideo();
            return true;
        }
        if (id == R.id.action_remove) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Are you Sure?")
                    .setMessage("Are you Sure You want to delete this Video from Your Device?")
                    .setCancelable(true)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            removeVideo();
                            dialog.cancel();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }


        return super.onOptionsItemSelected(item);
    }

    private void removeVideo() {
        File video = new File(getActivity().getFilesDir() + "/" + mFileName);
        video.setWritable(true, false);
        if (video.delete()) {
            Log.i(TAG, "File Deleted");
            getActivity().invalidateOptionsMenu();

        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (isFileDownloaded()) {
            inflater.inflate(R.menu.menu_video_view_remove, menu);

        } else {
            inflater.inflate(R.menu.menu_video_view, menu);
        }

        super.onCreateOptionsMenu(menu, inflater);

    }

    //endregion
    //region Download Video
    // Show Dialog Box with Progress bar
    private void createDialog() {
        prgDialog = new ProgressDialog(getActivity());
        prgDialog.setMessage("Downloading " + mVideoName + " Please wait...");
        prgDialog.setIndeterminate(false);
        prgDialog.setMax(100);
        prgDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        prgDialog.setCancelable(false);
        prgDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel Download", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                downloadTask.cancel(true);
//        File video = new File(getActivity().getFilesDir() + "/" + mFileName);
//        video.setWritable(true,false);
//        if(video.delete()){
//            Log.i(TAG,"File removed In Cleanup");
//        }
            }
        });
        prgDialog.show();
    }

    private void downloadVideo() {

        File newVideo;

        newVideo = new File(getActivity().getFilesDir(), mFileName);
        if (!newVideo.exists()) {
            downloadTask = new DownloadVideoTask();
            downloadTask.execute(mVidString);
            Log.i(TAG, "Made it Here");


        }

    }


    class DownloadVideoTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            createDialog();
            mFileDownloading = true;
            getActivity().invalidateOptionsMenu();

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            File video = new File(getActivity().getFilesDir() + "/" + mFileName);
            video.setWritable(true, false);
            if (video.delete()) {
                Log.i(TAG, "File removed In Cleanup");
            }
            fileIsDownloaded = false;
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL vidURL;
                vidURL = new URL(mVidString);


                long startTime = System.currentTimeMillis();
                Log.i(TAG, "Video Download Beginning" + vidURL);

                URLConnection ucon = vidURL.openConnection();
                int lengthOfFile = ucon.getContentLength();


                //Set TimeOut time
                ucon.setReadTimeout(TIMEOUT_CONNECTION);
                ucon.setConnectTimeout(TIMEOUT_SOCKET);

                //Input Stream - Read from the URL Connection
                // 3KB download buffer
                InputStream is = ucon.getInputStream();
                BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 10);


                FileOutputStream outStream = getActivity().openFileOutput(mFileName, Context.MODE_PRIVATE);
                Log.i(TAG, "OUTSTREAM: " + outStream);
                byte[] buff = new byte[1024];

                long total = 0;
                while ((count = inStream.read(buff)) != -1) {
                    total += count;

                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    outStream.write(buff, 0, count);
                    Log.i(TAG, "Count: " + count);
                }

                //Cleanup time

                outStream.flush();
                outStream.close();
                inStream.close();

            } catch (IOException e) {
                Log.d(TAG, "Error!" + e.getMessage());

            }


            return null;

        }

        @Override
        protected void onProgressUpdate(String... progress) {
            prgDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String file_url) {
            prgDialog.dismiss();
            mFileDownloading = false;
            getActivity().invalidateOptionsMenu();
            Log.i(TAG, "Download Done");
        }
    }
//endregion
}



