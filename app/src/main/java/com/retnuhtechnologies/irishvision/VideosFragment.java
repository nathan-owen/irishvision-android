package com.retnuhtechnologies.irishvision;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.DeleteCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;

import java.util.Date;
import java.util.List;

/**
 * Created by nathanowen on 3/4/15.
 */
public class VideosFragment extends ListFragment {
    private static final String TAG = VideosFragment.class.getSimpleName();
    private ParseQueryAdapter<ParseObject> messageAdapter;
    private VideosParseQueryAdapter customAdapter;
    protected List<String> mSubscribedChannels;
    protected List<ParseObject> mMessages;
    protected TextView mNoMessagesTextView;
    protected ListView mListView;
    private boolean loadedFromNetwork;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_videos, container, false);
Log.i(TAG,"Opened Videos!");
        mNoMessagesTextView = (TextView) rootView.findViewById(R.id.noMessages);
        mListView = (ListView) rootView.findViewById(android.R.id.list);

        getVideosLocally();
        if(checkConnection()){
            getVideosOnNetwork();
        }
        setHasOptionsMenu(true);


        return rootView;

    }

private void getVideosOnNetwork(){
    loadedFromNetwork = true;

    customAdapter = new VideosParseQueryAdapter(getActivity(),false);
    mListView.setAdapter(customAdapter);

    customAdapter.loadObjects();
    customAdapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<ParseObject>() {
        @Override
        public void onLoading() {

        }

        @Override
        public void onLoaded(final List<ParseObject> parseObjects, Exception e) {
            if (e == null) {
                if (parseObjects.isEmpty()) {
                    mNoMessagesTextView.setText("There are Currently No Videos");
                } else {
                    ParseObject.unpinAllInBackground(new DeleteCallback() {
                        @Override
                        public void done(ParseException e) {
                            ParseObject.pinAllInBackground("videos",parseObjects);
                        }
                    });
                    mNoMessagesTextView.setText("");
                }
            }

        }

    });



}
    private void getVideosLocally(){
        customAdapter = new VideosParseQueryAdapter(getActivity(),true);
        mListView.setAdapter(customAdapter);

        customAdapter.loadObjects();

        customAdapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<ParseObject>() {
            @Override
            public void onLoading() {

            }

            @Override
            public void onLoaded(final List<ParseObject> parseObjects, Exception e) {
                if (e == null) {
                    if(checkConnection()){
                        getVideosOnNetwork();
                    }
                    if (parseObjects.isEmpty()) {
                        mNoMessagesTextView.setText("There are Currently No Videos");
                    } else {

                        mNoMessagesTextView.setText("");
                    }
                }

            }

        });


    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        final Fragment newFragment = new VideoViewFragment();
        ParseObject selectedObject =  customAdapter.getItem(position);
        String selectedTitle =  selectedObject.getString(ParseConstants.KEY_VIDEO_NAME);
        String selectedUplDate =  selectedObject.getString(ParseConstants.KEY_VIDEO_UPL_DATE);
        String selectedVidUrl =  selectedObject.getString(ParseConstants.KEY_VIDEO_URL);
        String selectedImgUrl =  selectedObject.getString(ParseConstants.KEY_IMG_URL);
        Date selectedCreatedAt =  selectedObject.getCreatedAt();




        FragmentManager fragmentManager = getFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container,newFragment);
        fragmentTransaction.addToBackStack(null);
        //updateSelectedItem and title then close the drawer
        getActivity().setTitle(selectedTitle);
        final Bundle bundle = new Bundle();
        bundle.putString(ParseConstants.BUNDLE_VIDEO_NAME, selectedTitle);
        bundle.putString(ParseConstants.BUNDLE_VIDEO_URL, selectedVidUrl);
        bundle.putString(ParseConstants.BUNDLE_VIDEO_UPL_DATE, selectedUplDate);
        bundle.putString(ParseConstants.BUNDLE_IMG_URL, selectedImgUrl);
        bundle.putSerializable(ParseConstants.KEY_CREATED_AT, selectedCreatedAt);

        ParseFile imgFile = (ParseFile)selectedObject.get(ParseConstants.KEY_IMG_FILE);
        imgFile.getDataInBackground(new GetDataCallback() {
            @Override
            public void done(byte[] bytes, ParseException e) {
                if(e == null){
                    bundle.putByteArray(ParseConstants.KEY_IMG_FILE,bytes);
                    newFragment.setArguments(bundle);

                    fragmentTransaction.commit();


                }
                else{
                    Log.i(TAG,e.getMessage());
                }
            }
        });


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_refresh, menu);



        super.onCreateOptionsMenu(menu,inflater);

    }



    @Override
    public void onResume() {
        super.onResume();
refresh();
            getActivity().setTitle("IrishVision");



refresh();
    }
    private boolean checkConnection(){
        ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();


    }
    private void refresh() {
        if(loadedFromNetwork){
            customAdapter.loadObjects();
        }
        else {
            if(checkConnection()){
               getVideosOnNetwork();
            }
            else {
                getVideosLocally();
            }
            }
        Log.i(TAG, "refreshed");
    }




    }

