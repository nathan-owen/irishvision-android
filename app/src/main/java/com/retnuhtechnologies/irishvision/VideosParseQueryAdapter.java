package com.retnuhtechnologies.irishvision;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.util.Date;

import static android.text.format.DateUtils.MINUTE_IN_MILLIS;
import static android.text.format.DateUtils.WEEK_IN_MILLIS;

/**
 * Created by nathanowen on 3/4/15.
 */
public class VideosParseQueryAdapter extends ParseQueryAdapter<ParseObject>{


    private static final String TAG = VideosParseQueryAdapter.class.getSimpleName();
    private  static String filter = null;


    public VideosParseQueryAdapter(Context context, final boolean local) {
        // Use the QueryFactory to construct a PQA that will only show
        // Todos marked as high-pri
        super(context, new ParseQueryAdapter.QueryFactory<ParseObject>() {
            public ParseQuery create() {

                ParseQuery query = new ParseQuery(ParseConstants.CLASS_VIDEOS);
                if(local){
                    query.fromLocalDatastore();
                }
                query.orderByDescending(ParseConstants.KEY_CREATED_AT);
               // query.orderByDescending(ParseConstants.KEY_VIDEO_UPL_DATE);

                Log.i(TAG, query + "");
                return query;
            }
        });
        setObjectsPerPage(25);
    }


    // Customize the layout by overriding getItemView
    @Override
    public View getItemView(ParseObject object, View v, ViewGroup parent) {
        if (v == null) {
            v = View.inflate(getContext(), R.layout.video_row, null);
        }

        super.getItemView(object, v, parent);


Log.i(TAG,"Video Name: " + object.getObjectId() + "");
        // Add the title view
        TextView titleTextView = (TextView) v.findViewById(R.id.titleText);
        titleTextView.setText(object.getString(ParseConstants.KEY_VIDEO_NAME));
//Log.i(TAG, object.getString(ParseConstants.KEY_VIDEO_NAME));
        // Add a reminder of how long this item has been outstanding
        TextView timestampView = (TextView) v.findViewById(R.id.dateText);





        timestampView.setText(object.getString(ParseConstants.KEY_VIDEO_UPL_DATE));

        return v;
    }
}
