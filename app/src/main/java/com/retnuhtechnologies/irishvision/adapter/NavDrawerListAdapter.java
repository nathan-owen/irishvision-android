package com.retnuhtechnologies.irishvision.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.retnuhtechnologies.irishvision.R;
import com.retnuhtechnologies.irishvision.model.NavDrawerItem;

import java.util.ArrayList;

/**
 * Created by nathanowen on 3/2/15.
 */
public class NavDrawerListAdapter extends BaseAdapter {

    private Context mContext;

    private ArrayList<NavDrawerItem> mNavDrawerItems;
    public NavDrawerListAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems){
        mContext = context;
        mNavDrawerItems = navDrawerItems;
    }


    @Override
    public int getCount() {
        return mNavDrawerItems.size();
    }

    public Object getItem (int position){
        return mNavDrawerItems.get(position);


    }
    public long getItemId(int position){
        return position;

    }

    public View getView(int position, View convertView, ViewGroup parent){

        if(convertView == null){
            LayoutInflater mInflater = (LayoutInflater)
                    mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.drawer_list_item,null);
        }
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);

        txtTitle.setText(mNavDrawerItems.get(position).getTitle());

        return convertView;


    }



}
