package com.retnuhtechnologies.irishvision.model;

/**
 * Created by nathanowen on 3/2/15.
 */
public class NavDrawerItem {

    private String mTitle;
    public NavDrawerItem(){}

    public NavDrawerItem(String title){

        this.mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }


}
